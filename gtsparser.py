import sys
import json
from datetime import datetime
from ReadDataBaseXML import setListData
import FailCaseMapper

def readLog(str_Path):
    list_lines = []
    # read all lines in log as a list
    file = open(str_Path, 'r')
    for line in file:
        list_lines.append(line)
    file.close()
    return list_lines

def value(str, key):
    return str.partition(key)[2].split('"')[1]
    
def listResultline(list_log):
    list_result = []
    test_case = ''
    abi = ''
    suitePlan = ''
    suiteBuild = ''
    hostInfo = ''
    secutiryPatch = ''
    fingerPrint =''
    releaseSDK = ''

    for index in range(len(list_log)):
        # dict_result = [Date, TestCase, TestItem, Message, Detail]
        dict_result = {}

        if "build_device=" in list_log[index]:
            prj = list_log[index].split('"')[25];
            time = datetime.utcfromtimestamp(int(timestamp[:10]))
            fingerPrint = value(list_log[index], 'build_fingerprint')
            secutiryPatch = value(list_log[index], 'build_version_security_patch')
            releaseSDK = value(list_log[index], 'build_version_release') + ' (' + value(list_log[index], 'build_version_sdk') + ')'

        if "Result start=" in list_log[index]:
            timestamp = list_log[index].split('"')[1];
            time = datetime.utcfromtimestamp(int(timestamp[:10]))
            suitePlan = value(list_log[index], 'suite_name') + '/' + value(list_log[index], 'suite_plan')
            suiteBuild = value(list_log[index], 'suite_version') + '/' + value(list_log[index], 'suite_build_number')
            hostInfo = value(list_log[index], 'host_name') + ' (' + value(list_log[index], 'os_name') + ' - ' + value(list_log[index], 'os_version') + ')'

        if "TestCase name=" in list_log[index]:
            test_case = list_log[index].split('"')[1]

        if " abi=\"" in list_log[index]:
            abi = list_log[index].split('"')[3]

        if "Test result=\"fail\"" in list_log[index]:
            dict_result['SecutiryPatch'] = secutiryPatch
            dict_result['FingerPrint'] = fingerPrint
            dict_result['ReleaseSDK'] = releaseSDK
            dict_result['HostInfo'] = hostInfo
            dict_result['SuiteBuild'] = suiteBuild
            dict_result['SuitePlan'] = suitePlan
            dict_result['Project'] = prj
            dict_result['Date'] = str(time)
            dict_result['TestCase'] = test_case
            dict_result['abi'] = abi
            dict_result['TestItem'] = list_log[index].split('"')[3]
            dict_result['Message'] = list_log[index+1].split('"')[1].replace("'", "")
            dict_result['Detail'] = list_log[index+2].split('>')[1].replace("'", "")
            #print('dict_result is %s' % str(dict_result))
            list_result.append(dict_result)
    return list_result

def outputToFile(list_result):
    fp = open("output.txt", "w")
    fp.write("[\n")
    for info in list_result:
        #fp.write(json.dumps(info))
        fp.write(json.dumps(info, sort_keys=True, indent=4, separators=(',', ': ')))
        fp.write(",\n")
    fp.write("]")
    fp.close()

def main():
    print ('argument list: ', str(sys.argv))
    list_log = readLog(sys.argv[1])
    list_result = listResultline(list_log)
    setListData(list_result)

    #Mapping fail case to Jira db
    dbName = "test"
    testSuite = "gts"
    for item in list_result:
        testCase = item['TestCase']
        FailCaseMapper.mapping_data_and_insert_db(testCase, dbName, testSuite)
        
    #outputToFile(list_result)

if __name__ == '__main__':
    main()
