# -*- coding: utf-8 -*-
import pymysql

db = pymysql.connect(host='10.57.61.101',user='fihtdc',passwd='fihtdc',db='test', charset="utf8")
cursor = db.cursor()

def selectData(TestCase):
    #Calculate numbers of TestCase in row database
    try:
        result = cursor.execute('SELECT TestCase FROM gts_xml WHERE TestCase = "%s"' % \
                    (TestCase))
        print("---------------------------------")
        print("Select row numbers of keywords %s : %s" % (TestCase, result))
    except:
        db.rollback()

def updateData(TestItem, TestCase):
    try:
        result = ('update gts_xml set TestItem = "%s" where TestCase = "%s"' % \
                (TestItem, TestCase))
        cursor.execute(result)
        print(result)
    except:
        db.rollback()

def deleteRowData(TestCase):
    #delete data from database where TestCase is?
    try:
        cursor.execute('DELETE FROM gts_xml WHERE TestCase=("%s")' % \
                    (TestCase))
    except:
        db.rollback()
    print("Delete row data which TestCase is : " + TestCase)


def deleteAllData(tbl_name):
    #delete data from database where TestCase is?
    try:
        sql = 'TRUNCATE TABLE %s' % (tbl_name)
        cursor.execute(sql)
    except:
        db.rollback()
    print("---------------------------------")
    print("Delete all data from database -->" + tbl_name)


def insertData(TestCase, TestItem):
    # insert data to database
    try:
        cursor.execute('INSERT INTO gts_xml VALUES ("%s", "%s")' % \
                        (TestCase, TestItem))
    except:
        db.rollback()

def insertToDatabase(Project, Date, TestCase, TestItem, Message, Detail, SecutiryPatch, FingerPrint, ReleaseSDK, HostInfo, SuiteBuild, SuitePlan, abi):
    # insert data to databaseSELECT * FROM `gts_xml` WHERE 1SELECT * FROM `gts_xml` WHERE 1
    try:
        sql = "INSERT INTO gts_xml (Project, Date, TestCase, TestItem, Message, Detail, SecutiryPatch, FingerPrint, ReleaseSDK, HostInfo, SuiteBuild, SuitePlan, abi) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % \
                (Project, Date, TestCase, TestItem, Message, Detail, SecutiryPatch, FingerPrint, ReleaseSDK, HostInfo, SuiteBuild, SuitePlan, abi)
        print (sql)
        cursor.execute(sql)
    except:
        db.rollback()

#get List dictionary data from Henry
def setListData(List):
    #get List of dict to insert data to database
    print('------------------------------------------------------------------------------')
    for dict in List:
        insertToDatabase( dict.get('Project'), dict.get('Date'), dict.get('TestCase'), dict.get('TestItem'), dict.get('Message'), \
        dict.get('Detail'), dict.get('SecutiryPatch'), dict.get('FingerPrint'), dict.get('ReleaseSDK'), dict.get('HostInfo'), dict.get('SuiteBuild'), \
		dict.get('SuitePlan'), dict.get('abi'))

def printData(tbl_name):
    # print database data
    db = pymysql.connect(host='10.57.61.101',user='fihtdc',passwd='fihtdc',db='test')
    cursor = db.cursor()
    sql = 'SELECT * FROM `%s`' % tbl_name
    try:
        cursor.execute(sql)
        itemRows = cursor.fetchall()
        print("Query all data from database-->> " + tbl_name)
        for item in itemRows:
            print(item)
        print("---------------------------------")
    except:
        print("Error: unable to fecth data")
    db.close()

def main():
    #jira, jira_history_cts_fail

    #insertData('BBB','wiya','QA','test')
    #selectData('BBB')
    #deleteRowData('Darren')
    #updateData('a1', 'a1', 'a1', 'AAA')
    ##deleteAllData('gts_xml')
    printData('gts_xml')

if __name__ == '__main__':
    main()