# -*- coding: utf-8 -*-
import pymysql
import time
import os
import stat

class DBOperator():
    def __init__(self):
        self.dbName = "test"
        self.dbTable = "jira_history_cts_fail_with_pr"
        self.db = None
        self.cursor = None
        self.fp_error_log = None
        self.error_log_path = ""

        root = os.path.dirname(os.path.abspath(__file__))
        date_string = time.strftime("%Y%m%d", time.localtime())
        self.error_log_path = root + "/log/" + "db_error_" + date_string + ".log"
        if not os.path.exists(root + "/log"):
            os.makedirs(root + "/log")

        if not os.path.exists(self.error_log_path):
            self.fp_error_log = open(self.error_log_path, "w")
        else:
            self.fp_error_log = open(self.error_log_path, "a")
            self.errorLog("\n---------------------------------------------\n\n")
        
    def errorLog(self, text):
        print(text)
        self.fp_error_log.write(text + "\n")

    def updateDBName(self, dbName):
        # If without update, the default db is "test"
        print("Update database name: " + dbName)
        self.dbName = dbName

    def switchDBTable(self, dbTable):
        print("Switch database table to " + str(dbTable))
        self.dbTable = dbTable
    
    def databaseInitial(self):
        if self.fp_error_log is not None:
            self.fp_error_log.close()
            self.fp_error_log = open(self.error_log_path, "a")
        else:
            self.fp_error_log = open(self.error_log_path, "w")

        #Initial the database
        print("Connect to database. dbName:" + str(self.dbName))
        self.db = pymysql.connect(host='10.57.61.101',user='root',passwd='foxconn123',db=self.dbName, charset="utf8")
        self.cursor = self.db.cursor()

    def closeDB(self):
        self.cursor.close()
        self.db.close()
        self.fp_error_log.close()
        os.chmod(self.error_log_path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
        if os.stat(self.error_log_path).st_size == 0:
            os.remove(self.error_log_path)

    def selectAll(self):
        # Show all data in database
        sql = ""
        try:
            sql = 'SELECT * FROM ' + self.dbTable
            #print(sql)
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
            #for row in result:
            #    print(row)
            return result
        except:
            self.db.rollback()
            self.errorLog("Select data exception failed. SQL:" + "\\n".join(sql.splitlines()))


    def selectData(self, pr):
        # Show the selectd data in database
        try:
            sql = 'SELECT * FROM ' + self.dbTable + ' WHERE pr = "%s" ' % \
                    (pr)
            #print(sql)
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
            #for row in result:
            #    print(row)
            return result
        except:
            self.db.rollback()
            self.errorLog("Select data exception failed. SQL:" + "\\n".join(sql.splitlines()))


    def selectDataDynamic(self, selectDict):
        # Show the selectd data in database by dynamic
        sql = ""
        try:
            whereConditions = []
            for key, value in selectDict.items():
                cond = "%s = '%s'" % (key, value)
                whereConditions.append(cond)
            whereConditionSql = ' AND '.join(whereConditions) #Use join between comma in dict
            sql = 'SELECT * FROM ' + self.dbTable + ' WHERE %s ' % whereConditionSql
            #print(sql)
            self.cursor.execute(sql)
            result = self.cursor.fetchall()
            #for row in result:
            #    print(row)
            return result
        except:
            self.db.rollback()
            self.errorLog("Select data exception failed. SQL:" + "\\n".join(sql.splitlines()))
    
    def updateData(self, set_dict, where_dict):
        sql = ""
        set_list_str = ""
        where_list_str = ""

        keys = set_dict.keys()
        for key in keys:
            set_list_str = set_list_str + key + "=" + "\"" + str(set_dict[key]).replace("\"", "'").replace("\\", "\\\\") + "\", "
        set_list_str = set_list_str[:-2]

        keys = where_dict.keys()
        for key in keys:
            where_list_str = where_list_str + key + "=" + "\"" + str(where_dict[key]).replace("\"", "'").replace("\\", "\\\\") + "\", "
        where_list_str = where_list_str[:-2]

        try:
            sql = "UPDATE " + self.dbName + "." + self.dbTable + " " +\
                    "SET " + set_list_str +\
                    "WHERE " + where_list_str
            print("\\n".join(sql.splitlines()))
            self.cursor.execute(sql)
            self.db.commit()
        except:
            self.db.rollback()
            self.errorLog("Update data exception failed. SQL:" + "\\n".join(sql.splitlines()))
    
    
    def deleteAllData(self, tbl_name):
        #delete data from database where owner is?
        try:
            sql = 'TRUNCATE TABLE %s' % (tbl_name)
            self.cursor.execute(sql)
        except:
            self.db.rollback()
        print("---------------------------------")
        print("Delete all data from database -->" + tbl_name)


    def insertData(self, insert_dict):
        sql = ""
        key_list_str = "("
        value_list_str = "("

        keys = insert_dict.keys()
        for key in keys:
            key_list_str = key_list_str + str(key) + ", "
            value_list_str = value_list_str + "\""  + str(insert_dict[key]).replace("\"", "'").replace("\\", "\\\\") + "\", "

        key_list_str = key_list_str[:-2] + ")"
        value_list_str = value_list_str[:-2] + ")"

        try:
            sql = "INSERT INTO " + self.dbName + "." + self.dbTable + " " +\
                    key_list_str + " VALUES " + value_list_str
            print("\\n".join(sql.splitlines()))
            self.cursor.execute(sql)
            self.db.commit()
        except:
            self.db.rollback()
            self.errorLog("Insert data exception failed. SQL:" + "\\n".join(sql.splitlines()))
        
    
    def printData(self, tbl_name):
        # print database data
        sql = 'SELECT * FROM `%s`' % tbl_name
        try:
            self.cursor.execute(sql)
            itemRows = self.cursor.fetchall()
            print("Query all data from database-->> " + tbl_name)
            for item in itemRows:
                print(item)
            print("---------------------------------")
        except:
            self.errorLog("Error: unable to fecth data")


# if __name__ == '__main__':
    # dict = {
        # 'pr' : 'B2N',
        # 'pr_number' : 'B2N-1896',
        # 'key_word' : 'CTS',
        # 'status' : 'Closed'
    # }
    # dbOP = DBOperator()
    # dbOP.databaseInitial()
    # dbOP.selectDataDynamic(dict)
    # dbOP.closeDB()
