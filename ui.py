import tkinter as tk
import json
import parsing
from ReadDataBaseXML import setListData

def startParse():
    project = entry_project.get()
    text = entry_searchText.get()
    count = entry_count.get()

    infoList = parsing.getIssueInfoList(text, project, count)
    outputToFile(infoList)#Debug purpose, won't output to file in the furture.
    #TODO: Pass infoList to Darren
    setListData(infoList) #Add by Darren

    return infoList

def outputToFile(infoList):
    fp = open("output.txt", "w")
    fp.write("[\n")
    for info in infoList:
        fp.write(json.dumps(info))
        fp.write(",\n")
    fp.write("]")
    fp.close()

if __name__ == '__main__':
    root = tk.Tk()
    root.title("Issue Query & Parse")
    tk.Label(root, text="Project").grid(row=0, padx=2, pady=2,)
    tk.Label(root, text="Search text").grid(row=1, padx=2, pady=2)
    tk.Label(root, text="Search count ").grid(row=2, padx=2, pady=2)

    entry_project = tk.Entry(root)
    entry_searchText = tk.Entry(root)
    entry_count = tk.Entry(root)
    entry_project.grid(row=0, column=1)
    entry_searchText.grid(row=1, column=1)
    entry_count.grid(row=2, column=1)

    btn_parse = tk.Button(root, text="Start", command=startParse).grid(row=2, column=2, padx=5, sticky="WE")

    root.mainloop()
